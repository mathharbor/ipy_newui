===========================================
 IPython: Productive Interactive Computing
===========================================


Overview
========

Our fork of the IPython Notebook, with significant UI modifications and enhancements.

Development installation
========================

If you want to hack on certain parts in a clean
environment (such as a virtualenv) you can use ``pip`` to grab the necessary
dependencies quickly::

   $ git clone --recursive https://mathharbor@bitbucket.org/mathharbor/ipy_newui.git
   $ cd ipython
   $ pip install -e ".[notebook]"

This installs the necessary packages and symlinks IPython into your current
environment so that you can work on your local repo copy and run it from anywhere::

   $ ipython notebook --pylab=inline --ip=0.0.0.0 --port=8000


